<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function create(){
        return view('article.create');
    }

    public function articleShow(Article $article){
        
        return view('article.show', compact('article'));
    }

    public function articleIndex(){
        
        $articles = Article::where('is_accepted', true)->paginate(12);
        
        return view('article.index', compact('articles'));
        
    }

    public function edit(Article $article){
        if(Auth::user()->id == $article->user_id){
            return view('article.edit', compact('article'));
        } 
        return redirect()->back()->with('denied', 'Accesso non consentito');
    }
}
