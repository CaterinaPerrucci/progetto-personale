<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class PublicController extends Controller
{
    public function home() {

        $articles = Article::where('is_accepted', true)->orderBy('created_at', 'desc')->take(6)->get();

        return view('welcome', compact('articles'));
    }

    public function categoryShow(Category $category){

        return view('category.show', compact('category'));
    }

    public function searchArticles(Request $request){

        $articles = Article::search($request->searched)->where('is_accepted', true)->paginate(12);

        return view('article.index', compact('articles'));
    }

    public function setLanguage($lang){

        session()->put('locale', $lang);
        
        return redirect()->back();
    }


    public function redirectToProvider()
    {

    return Socialite::driver('github')->redirect();

    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->stateless()->user();
        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name' => $user->name,
            'password' => Hash::make(Str::random(8))
        ]);
        Auth::login($user, true);
        return redirect('/')->with('message', 'Ti sei loggato correttamente con Github');
    }

    public function google()
    {

    return Socialite::driver('google')->redirect();

    }

    public function googleCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name' => $user->name,
            'password' => Hash::make(Str::random(8))
        ]);
        Auth::login($user, true);
        return redirect('/')->with('message', 'Ti sei loggato correttamente con Google');
    }

    public function dashboard(User $user, Article $article){
        if(Auth::user()->id == $user->id){  
            return view('auth.dashboard', compact('user'));
        } 
        return redirect()->back()->with('denied', 'Non puoi accedere alla dashboard di un altro utente');
    }
}
