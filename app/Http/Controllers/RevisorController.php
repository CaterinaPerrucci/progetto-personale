<?php

namespace App\Http\Controllers;

use App\Mail\AcceptMail;
use App\Models\User;
use App\Models\Article;
use App\Mail\BecomeRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function dashboard(){

        $article_to_check = Article::where('is_accepted', null)->where('user_id', '!=', Auth::user()->id)->paginate(1);

        return view('revisor.dashboard', compact('article_to_check'));
    }

    public function acceptArticle(Article $article){

        $article->setAccepted(true);
        Mail::to(Auth::user()->email)->send(new AcceptMail(Auth::user()->name));

        return redirect()->back()->with('message', "Complimenti, hai accettato l'annuncio");
    }

    public function rejectArticle(Article $article){

        $article->setAccepted(false);

        return redirect()->back()->with('message', "Complimenti, hai rifiutato l'annuncio");
    }

    public function reviewArticle(Article $article){

        $article->setAccepted(null);

        return redirect()->back()->with('message', "Complimenti, hai rimandano in revisione l'annuncio");
    }

    public function becomeRevisor(Request $request){

        Mail::to('admin@presto.it')->send(new BecomeRevisor(Auth::user(), $request->description));
        
        return redirect()->back()->with('message', "Complimenti! La tua richiesta è andata a buon fine");
    }

    public function makeRevisor(User $user){

        Artisan::call('presto:makeUserRevisor', ['email' => $user->email]);
        
        return redirect('/')->with('message', "Complimenti! L'utente è diventato un revisore");
    }

    public function becomeRevisorForm(){
        if(!Auth::user()->is_revisor){
            return view('revisor.form');
        }
        redirect()->route('welcome')->with('denied', 'Sei già un revisore');
    }
}
