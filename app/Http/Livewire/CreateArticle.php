<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateArticle extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $price;
    public $category;
    public $temporary_images;
    public $images = [];
    public $article;

    protected $rules = [

        'title' => 'required|min:3|max:20',
        'body' => 'required|min:10|max:1000',
        'price' => 'required|numeric|min:1|max:999999,99',
        'category' => 'required',
        'temporary_images.*' => 'image|max:5120',
        'images.*' => 'image|max:5120',
    ];

    protected $messages = [

        'required' => 'Il campo :attribute è obbligatorio',
        'min' => 'Non ci sono abbastanza caratteri',
        'max' => 'Numero massimo di caratteri superato',
        'number' => 'Il campo deve essere un numero',
        'image' => 'Il file dev\'essere un\'immagine',
        'temporary_images.*.max' => 'L\'immagine non deve superare 5mb',
        'images.*.max' => 'L\'immagine non deve superare 5mb',
    ];

    public function updatedTemporaryImages(){

        if($this->validate([
            'temporary_images.*' => 'image|max:5120',
        ])) {
            foreach($this->temporary_images as $image){
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){

        if(in_array($key, array_keys($this->images))){
            unset($this->images[$key]);
        }
    }

    public function updated($propertyName){

        $this->validateOnly($propertyName);
    }

    public function store(){

        $this->validate();

        $this->article = Category::find($this->category)->articles()->create($this->validate());

            if(count($this->images)){
                foreach($this->images as $image){
                    
                    $newFileName = "articles/{$this->article->id}";
                    $newImage = $this->article->images()->create(['path' => $image->store($newFileName, 'public')]);
                    RemoveFaces::withChain([
                        new ResizeImage($newImage->path, 600, 600),
                        new GoogleVisionSafeSearch($newImage->id),
                        new GoogleVisionLabelImage($newImage->id),
                    ])->dispatch($newImage->id);
                }

                File::deleteDirectory(storage_path('/app/livewire-tmp'));
            }
        
        $this->article->user()->associate(Auth::user());
        $this->article->save();

        $this->cleanForm();

        return redirect()->route('welcome')->with('messaggio', 'messaggio');
    }

    public function cleanForm(){

        $this->title = '';
        $this->body = '';
        $this->price = '';
        $this->category = '';
        $this->images = [];
        $this->temporary_images = [];
    }
  
    public function render()
    {
        $categories=Category::all();
        
        return view('livewire.create-article', compact('categories'));
    }
}
