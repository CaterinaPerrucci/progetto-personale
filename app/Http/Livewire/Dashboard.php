<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Article;
use App\Models\User;

class Dashboard extends Component
{
    public $user;
    public $article;
    public $modalTitle = "Conferma cancellazione";
    public $modalBody = "Attenzione! Sei sicuro di voler eliminare definitivamente l'annuncio?";

    public function article_destroy(Article $article){
        $article->delete();
        
        return redirect(route('welcome'))->with('message', 'Articolo eliminato definitivamente');
    }
    
    public function render()
    {
        return view('livewire.dashboard');
    }
}
