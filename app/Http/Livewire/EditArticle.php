<?php

namespace App\Http\Livewire;

use App\Models\Image;
use App\Models\Article;
use Livewire\Component;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\File;

class EditArticle extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $price;
    public $category;
    public $temporary_images;
    public $newImages = [];
    public $newImage;
    public $articleId;

    protected $rules = [

        'title' => 'required|min:3|max:20',
        'body' => 'required|min:10|max:1000',
        'price' => 'required|numeric|min:1|max:999999,99',
        'category' => 'required',
        'temporary_images.*' => 'image|max:5120',
        'images.*' => 'image',
    ];

    protected $messages = [

        'required' => 'Il campo :attribute è obbligatorio',
        'min' => 'Non ci sono abbastanza caratteri',
        'max' => 'Numero massimo di caratteri superato',
        'number' => 'Il campo deve essere un numero',
        'image' => 'Il file dev\'essere un\'immagine',
        'temporary_images.*.max' => 'L\'immagine non deve superare 5mb',

    ];

    protected $listeners = [
        'refresh' => '$refresh'
    ];

    public function updated($propertyName){

        $this->validateOnly($propertyName);
    }

    public function updatedTemporaryImages(){

        if($this->validate([
            'temporary_images.*' => 'image|max:5120',
        ])) {
            foreach($this->temporary_images as $image){
                $this->newImages[] = $image;
            }
        }
    }

    public function removeImage($key){

        if(in_array($key, array_keys($this->images))){
            unset($this->images[$key]);
        }
    }

    public function removeOldImage(Image $image){
        $image->delete();
        //cancellare storage
        $this->emitSelf('refresh');
    }

    public function article_update(){
        $article = Article::find($this->articleId);
        
        $article->update([
          'title' => $this->title,
          'body' => $this->body,
          'price' => $this->price,
          'category' => $this->category,
          'images' => $this->images,
        ]);
        
        if(count($this->newImages)){
            foreach($this->newImages as $newImage){
                $newFileName = "articles/{$article->id}";
    

                $newImage = $article->images()->create([
                    'path' => $newImage->store($newFileName, 'public')
                ]);
                
                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 600, 600),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                ])->dispatch($newImage->id);
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }
        return redirect(route('article.index'));
    }

    public function article_destroy(){
        $article = Article::find($this->articleId);
        $article->delete();

        return redirect(route('article.index'));
    }

    public function mount(){
        $article = Article::find($this->articleId);
        $this->title = $article->title;
        $this->body = $article->body;
        $this->price = $article->price;
        $this->category = $article->category_id;
        $this->images = $article->images;
    }

    public function render()
    {
        return view('livewire.edit-article');
    }
}
