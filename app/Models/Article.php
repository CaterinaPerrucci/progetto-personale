<?php

namespace App\Models;

use App\Models\User;
use App\Models\Image;
use App\Models\Category;
use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory, Searchable;

    protected $fillable = ['title', 'body', 'price', 'category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function setAccepted($value){

        $this->is_accepted = $value;
        $this->save();

        return ;
    }

    public static function toBeRevisionedCount(){

        return Article::where('is_accepted', null)->where('user_id', '!=', Auth::user()->id)->count();
    }

    public function images(){
        
        return $this->hasMany(Image::class);
    }

    public function toSearchableArray(){

        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'category' => $this->category,
        ];
        return $array;
    }
}
