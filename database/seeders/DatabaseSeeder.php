<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'User',
            'email' => 'test@example.com',
            'password' => Hash::make('12345678'),
            'is_revisor' => 1,
        ]);

        $categories = [
            'Elettronica', 'Arredamento', 'Abbigliamento', 'Cucina', 'Bellezza', 'Salute', 'Sport', 'Hobby', 'Infanzia', 'Animali'
        ];

        foreach($categories as $category){
            Category::create([
                'name' => $category
            ]);
        }
    }
}
