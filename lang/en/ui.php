<?php

return [
    'lastProducts' => 'Last products',
    'allProducts' => 'All products',
    'categories' => 'Categories',
    'register' => 'Register',
    'search' => 'Search',
    'language' => 'Languages',
    'insert' => 'Insert announcement',
    'revisor' => 'Reviewer area',
    'messaggio' => 'Article created successfully, awaiting review',
    'Elettronica' => 'Tech'


];