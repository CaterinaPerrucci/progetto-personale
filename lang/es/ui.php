<?php

return [
    'lastProducts' => 'Los últimos productos',
    'allProducts' => 'Todos los productos',
    'categories' => 'Categorías',
    'register' => 'Regístrate',
    'search' => 'Busca',
    'language' => 'Idiomas',
    'insert' => 'Publica anuncio',
    'revisor' => 'Zona revisor',
    'messaggio' => 'Producto creado correctamente, pendiente de revisión',
    'Elettronica' => 'Electrónica'


];