<?php

return [
    'lastProducts' => 'Gli ultimi prodotti',
    'allProducts' => 'Tutti i prodotti',
    'categories' => 'Categorie',
    'register' => 'Registrati',
    'search' => 'Cerca',
    'language' => 'Lingue',
    'insert' => 'Inserisci annuncio',
    'revisor' => 'Zona revisore',
    'messaggio' => 'Annuncio creato con successo, in attesa di revisione',
    'Elettronica' => 'Elettronica'



];