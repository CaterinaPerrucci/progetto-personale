import './bootstrap';
import 'bootstrap/dist/js/bootstrap.min.js';

const modalButtons = document.querySelectorAll('[data-bs-toggle="modal"]');
const confirmationButton = document.querySelector('#confirmationButton');

for(let modalButton of modalButtons) {

    modalButton.addEventListener('click', e => {

        const action = e.target.getAttribute('data-action');

        confirmationButton.setAttribute('wire:click', action);

    });

};