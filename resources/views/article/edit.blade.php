<x-layout>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <livewire:edit-article articleId="{{$article->id}}"/>
            </div>
        </div>
    </div>
</x-layout>