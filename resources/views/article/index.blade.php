<x-layout>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>Tutti gli annunci</h2>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row justify-content-around">
            @forelse($articles as $article)
                <div class="col-12 d-flex justify-content-center col-md-3 m-5">
                    <x-card :article="$article"/>
                </div>
            @empty
                <div class="col-12">
                    <h4>Non ci sono annunci caricati</h4 class="card-title">
                    @auth
                    <p>Pubblicane uno: <a href="{{route('article.create')}}" class="card-title">Inserisci annuncio</a></p>
                    @endauth
                </div>
            @endforelse    
            <div class="d-flex justify-content-center">{{$articles->links()}}</div>
        </div>
    </div>
</x-layout>