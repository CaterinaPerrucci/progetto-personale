<x-layout>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">{{$article->title}}</h2>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row">
            @forelse($article->images as $image)
                <div class="col-12 col-md-1">
                    <img src="{{$image->getUrl(600, 600)}}" class="w-100 my-3" alt="{{$article->title}}">
                </div>
                <div class="col-12 col-md-5">
                    <img src="{{$image->getUrl(600, 600)}}" class="w-100 my-3" alt="{{$article->title}}">
                </div>
            @empty
                <div class="col-12 col-md-1">
                    <img src="{{Storage::url( './../../img/default-image.jpg' )}}" class="w-100 my-3" alt="{{$article->title}}">
                </div>
                <div class="col-12 col-md-5">
                    <img src="{{Storage::url( './../../img/default-image.jpg' )}}" class="w-100 my-3" alt="immagine di default">
                </div>
            @endforelse
            <div class="col-12 col-md-6">
                <p class="display-6">€ {{$article->price}}</p>
                <a href="{{route('category.show', ['category' => $article->category])}}" class="card-title">{{$article->category->name}}</a>
                <h5>Descrizione:</h5>
                <div class="overflow-auto h-25 bg-white">
                    {{$article->body}}
                </div>
                <p>Inserito il: {{$article->created_at->format('d/m/Y')}}</p>
                <p>Caricato da: {{$article->user->name}}</p>
                @if (Auth::user())
                    <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{ $article->id }}" name="id">
                        <input type="hidden" value="{{ $article->title }}" name="name">
                        <input type="hidden" value="{{ $article->price }}" name="price">
                       
                        <input type="hidden" value="1" name="quantity">
                        <button class="btn btn-card m-2">Aggiungi al carrello</button>
                    </form>
                @endif
                @if(Auth::user() && Auth::user()->is_revisor)
                    <form action="{{route('revisor.review-article', $article)}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <button type="submit" class="btn btn-card m-2">Rimanda in revisione</button>
                    </form>
                    @if ($article->user_id == Auth()->user()->id)
                        <a href="{{route('article.edit', compact('article'))}}" class="btn btn-warning mt-3 btn-card m-2">Modifica articolo</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
</x-layout>