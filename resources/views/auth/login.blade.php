<x-layout>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="form">
                <span class="form-title">Login</span>
                <form action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="mb-3 form-input">
                        <i class="fa-regular fa-envelope"></i>
                        <input type="email" name="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <i class="fa-solid fa-lock"></i>
                        <input type="password" name="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
                        <span class="bar"></span>
                    </div>
                    <button type="submit" class="form-button">Accedi</button>
                    <p class="text-secondary text-center">Oppure accedi con GitHub o Google</p>
                    <div class="w-full d-flex justify-content-center">
                        <a href="{{ url('/login/github') }}" class="text-decoration-none">
                            <button class="form-button mt-0 p-2" type="button">
                                <i class="fa-brands fa-github"></i>
                            </button>
                        </a>
                        <a href="{{ url('/login/google') }}" class="text-decoration-none">
                            <button class="form-button mt-0 p-2" type="button">
                                <i class="fa-brands fa-google"></i>
                            </button>
                        </a>
                    </div>
                    <hr class="text-secondary">
                    <span class="form-switch text-center">Non sei registrato? <a href="{{route('register')}}">Registrati</a></span>
                </form>
            </div>
        </div>
    </div>
</x-layout>