<x-layout>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="form">
                <span class="form-title">Registrati</span>
                <form action="{{route('register')}}" method="POST">
                    @csrf
                    <div class="mb-3 form-input">
                        <i class="fa-regular fa-user"></i>
                        <input name="name" type="text" class="form-control" placeholder="Nome">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <i class="fa-regular fa-envelope"></i>
                        <input name="email" placeholder="Email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <i class="fa-solid fa-lock"></i>
                        <input name="password" type="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <i class="fa-solid fa-lock"></i>    
                        <input name="password_confirmation" placeholder="Conferma password" type="password" class="form-control" id="exampleInputPassword2">
                        <span class="bar"></span>
                    </div>
                    <button type="submit" class="form-button">Registrati</button>
                    <p class="text-secondary text-center">Oppure registrati con GitHub o Google</p>
                    <div class="w-full d-flex justify-content-center">
                        <a href="{{ url('/login/github') }}" class="text-decoration-none">
                            <button class="form-button mt-0 p-2" type="button">
                                <i class="fa-brands fa-github"></i>
                            </button>
                        </a>
                        <a href="{{ url('/login/google') }}" class="text-decoration-none">
                            <button class="form-button mt-0 p-2" type="button">
                                <i class="fa-brands fa-google"></i>
                            </button>
                        </a>
                    </div>
                    <hr class="text-secondary">
                    <span class="form-switch text-center">Hai già un account? <a href="{{route('login')}}">Login</a></span>
                </form>
            </div>
        </div>
    </div>
</x-layout>