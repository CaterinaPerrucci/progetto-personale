<x-layout>
    <div class="container my-5">
        <div class="row">
            <h2>Carrello</h2>
        </div>
    </div>
    <div class="container mx-auto">
        <div class="row d-flex justify-content-center my-5">
            <div class="col-12 col-md-6 w-100 p-5 bg-white shadow-lg">
                <table class="w-100 text-sm" cellspacing="0">
                    <thead>
                        <tr class="uppercase">
                            <th class="text-left">Nome articolo</th>
                            <th class="pl-5 text-left lg:text-right lg:pl-0">
                                <span class="hidden lg:inline">Quantità</span>
                            </th>
                            <th class="hidden text-right md:table-cell">Prezzo</th>
                            <th class="hidden text-right md:table-cell">Rimuovi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cartItems as $article)
                            <tr>
                                <td>
                                    <a href="{{route('article.show', $article->id)}}" class="text-decoration-none">
                                        <p class="mb-2 card-title">{{ $article->name }}</p>
                                    </a>
                                </td>
                                <td class="justify-content-center mt-6 md:justify-end md:flex">
                                    <div class="h-10 w-28">
                                        <div class="row justify-content-center">
                                            <form action="{{ route('cart.update') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $article->id}}" >
                                                <input type="text" name="quantity" value="{{ $article->quantity }}" class="w-16 text-center h-6 text-gray-800 outline-none rounded border border-blue-600" />
                                                <button class="btn btn-card">+</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                <td class="hidden text-right md:table-cell">
                                    <span class="text-sm font-medium lg:text-base">
                                        € {{ $article->price }}
                                    </span>
                                </td>
                                <td class="hidden text-right md:table-cell">
                                    <form action="{{ route('cart.remove') }}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{ $article->id }}" name="id">
                                        <button class="btn btn-card"><i class="fa-regular fa-trash-can"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach 
                    </tbody>
                </table>
                    <div>
                        Totale: € {{ Cart::getTotal() }}
                    </div>
                    <div>
                        <form action="{{ route('cart.clear') }}" method="POST">
                            @csrf
                            <button class="btn btn-card">Svuota Carrello</button>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</x-layout>