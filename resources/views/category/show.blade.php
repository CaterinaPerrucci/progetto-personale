<x-layout>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>{{$category->name}}</h2>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row">
            @forelse($category->articles as $article)
                <div class="col-12 d-flex justify-content-center col-md-3 my-2">
                    <x-card :article="$article"/>
                </div>
            @empty
                <div class="col-12">
                    <h4>Non ci sono annunci per questa categoria</h4 class="card-title">
                    @auth
                    <p>Pubblicane uno: <a href="{{route('article.create')}}" class="card-title">Inserisci annuncio</a></p>
                    @endauth
                </div>
            @endforelse    
        </div>
    </div>
</x-layout>