<form action="{{route('set_language_locale', $lang)}}" method="POST">
    @csrf
    <button type="submit" class="dropdown-link text-dark bg-transparent border-0">
        <span>{{ Str::upper($lang) }}</span>
    </button>
</form>