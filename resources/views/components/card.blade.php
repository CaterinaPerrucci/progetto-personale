<div class="card my-card rounded-0" style="width: 18rem;">
  <div class="photo">
    <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(600, 600) : 'https://picsum.photos/200'}}">
    <a class="text-decoration-none" href="{{route('category.show', ['category' => $article->category])}}"><div class="photos">{{$article->category->name}} </div><a >
  </div>
  <div class="card-body">
    <h5>{{$article->title}}</h5>
    <p class="card-text">€ {{$article->price}}</p>
    <a href="{{route('article.show', $article)}}" class="btn btn-card">Vai al dettaglio</a>
  </div>
</div>