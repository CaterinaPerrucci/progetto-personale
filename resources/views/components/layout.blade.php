<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        @livewireStyles
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <title>{{$title ?? 'Presto.it'}}</title>
    </head>
    <body>
        <x-navbar/>
        
        @if(Route::currentRouteName() == 'welcome') 
            <x-masthead/> 
        @endif
        
        <x-message />

        <div class="min-vh-100">
            {{$slot}}
        </div>

        <x-footer/>
        
        @livewireScripts
    </body>
</html>