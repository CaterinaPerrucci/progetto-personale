<div class="position-relative overflow-hidden p-3 p-md-5 text-center bg-light my-masthead">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
      <h1 class="display-2 lean ">Presto.it</h1>
      <p class="lead fw-normal">Lo shopping a portata di click</p>
      @if(!Auth::user())
        <a class="btn btn-masthead" href="{{route('login')}}">Acquista ora</a>
      @else
        <a class="btn btn-masthead" href="{{route('article.index')}}">Acquista ora</a>
      @endif
    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>