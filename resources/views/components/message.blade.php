<div>
    @if (session()->has('messaggio'))
        <div class="alert alert-success ">
            {{ __('ui.messaggio') }}
        </div>
    @elseif(session('access.denied'))
        <div class="alert alert-danger">
            {{session('access.denied')}}.
            Per diventare revisore <a href="{{route('become.revisor')}}" class="card-title">vai </a>al form.
        </div>
    @elseif(session('denied'))
        <div class="alert alert-danger">
            {{session('denied')}}.
        </div>
    @elseif(session('success'))
        <div class="alert alert-success ">
            {{session('success')}}
        </div>
    @endif
</div>