<nav class="navbar my-navbar navbar-expand-lg">
  <div class="container-fluid">
    <a class="navbar-brand logo" href="/"><i class="fa-solid fa-truck-fast"></i><span class="fst-italic fw-bold fs-5">P</span></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{__('ui.language')}}
            </a>
          <ul class="dropdown-menu">
            <li class="dropdown-item">
              <x-_locale lang='it' nation='it'/>
            </li>
            <li class="dropdown-item">
              <x-_locale lang='en' nation='gb'/>
            </li>
            <li class="dropdown-item">
              <x-_locale lang='es' nation='es'/>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'article.index') active @endif" aria-current="page" href="{{route('article.index')}}">{{__('ui.allProducts')}}</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle @if(Route::currentRouteName() == 'category.show') active @endif" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          {{__('ui.categories')}}
          </a>
          <ul class="dropdown-menu">
            @foreach($categories as $category)
              <li><a class="dropdown-item" href="{{route('category.show', $category)}}">{{__('ui.'.$category->name)}}</a></li>
            @endforeach
          </ul>
        </li>
        @guest
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'register') active @endif" href="{{route('register')}}">{{__('ui.register')}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'login') active @endif" href="{{route('login')}}">Login</a>
          </li>
        @endguest
        @auth
          <li class="nav-item">
            <a class="nav-link @if(Route::currentRouteName() == 'article.create') active @endif" href="{{route('article.create')}}">{{__('ui.insert')}}</a>
          </li>
          <li class="nav-item">
            <a href="{{route('user.dashboard', Auth::user())}}" class="nav-link">{{Auth::user()->name}}</a>
          </li>
          <li>
            <form action="{{route('logout')}}" method="POST">
              @csrf
              <button class="my-button">Logout</button>
            </form>
          </li>
          <li class="nav-item">
          <a href="{{ route('cart.list') }}" class="nav-link">
            <i class="fa-solid fa-cart-shopping"></i>
            <span>{{ Cart::getTotalQuantity()}}</span> 
          </a>
          </li>
        @endauth
      </ul>
      <ul class="navbar-nav">
      @if(Auth::user() && Auth::user()->is_revisor)
          <li class="nav-item me-3">
            <a href="{{route('revisor.dashboard')}}" class="@if(Route::currentRouteName() == 'revisor.dashboard') active @endif nav-link position-relative ms-2" aria-current="page">
              <span>{{__('ui.revisor')}}</span>
            </a>
            <span class="position-absolute top-50 start-75 translate-middle badge rounded-pill count">
              {{App\Models\Article::toBeRevisionedCount()}}
            </span>
          </li>
        @endif
      </ul>
      <form class="d-flex" role="search" action="{{route('article.search')}}" method="GET">
        <input name="searched" class="form-control me-2" type="search" placeholder="{{__('ui.search')}}..." aria-label="Search">
        <button class="my-button" type="submit">{{__('ui.search')}}</button>
      </form>
    </div>
  </div>
</nav>