<div class="form min-vh-100">
<span class="form-title">I tuoi annunci</span>
    <p class="ms-3">Numero di annunci inseriti: {{count($user->articles)}}</p>
    @if (count($user->articles) > 0)
        <table class="table table-danger table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Titolo</th>
                    <th scope="col">Modifica</th>
                    <th scope="col">Elimina</th>
                    <th scope="col">Stato</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user->articles as $article)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $article->title }}</td>
                        <td>
                        @if (!$article->is_accepted == true)
                            <a href="{{route('article.edit', compact('article'))}}" class="btn btn-warning btn-card">Modifica</a></td>
                            @else
                            <button class="btn btn-outline-secondary btn-card disabled text-decoration-line-through">Modifica</button>
                        @endif
                        <td>
                        @if (!$article->is_accepted == true)
                        <button type="button" class="btn btn-danger btn-card" data-bs-toggle="modal" data-bs-target="#modal" data-action="article_destroy({{$article}})">Elimina</button>
                        @else
                            <button class="btn btn-outline-secondary btn-card disabled text-decoration-line-through">Elimina</button>
                        @endif</td>
                        @if ($article->is_accepted == true)
                            <td class="text-success">Pubblicato</td>
                        @elseif(is_null($article->is_accepted))
                            <td class="text-warning">In revisione</td>
                        @else
                            <td class="text-danger">Rifiutato</td>
                        @endif
                    </tr>

                    <x-modal :$modalTitle :$modalBody :$article/>
                @endforeach
            </tbody>
        </table>
    @endif
    
</div>
