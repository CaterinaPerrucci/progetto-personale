<div class="form">
<span class="form-title">Modifica l'annuncio</span>
    <form wire:submit.prevent="article_update">
        <div class="mb-3">
            <label class="form-label">Titolo dell'articolo</label>
            <input type="text" placeholder="Titolo dell'articolo" class="form-control @error('title') is-invalid @enderror" wire:model="title">
            @error('title')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Categoria</label>
            <select wire:model.defer="category" id="" class="form-control @error('category') is-invalid @enderror">
                <option value="">Scegli la categoria</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            @error('category')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Descrizione</label>
            <textarea wire:model="body" cols="30" rows="5" placeholder="Descrizione..." class="form-control @error('body') is-invalid @enderror"></textarea>
            @error('body')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Prezzo</label>
            <input type="number" step="0.01" class="form-control @error('price') is-invalid @enderror" placeholder="€" wire:model="price">
            @error('price')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        
        @if(!empty($images))
            <div class="row mb-3">
                <div class="col-12">
                <label class="form-label">Immagini</label>
                    <div class="row my-border rounded shadow py-4">
                        @foreach($images as $image)
                            <div class="col my-3">
                                <div class="img-preview mx-auto shadow" style="background-image:url({{$image->getUrl()}});">
                                </div>
                                <button type="button" class="btn btn-card shadow d-block text-center mt-3 mx-auto" wire:click="removeOldImage({{$image}})">Cancella</button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="mb-3">
            <label class="form-label">Nuove immagini</label>
            <input wire:model="temporary_images" type="file" multiple class="form-control @error('temporary_images.*') is-invalid @enderror" placeholder="Immagini">
            @error('temporary_images.*')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        @if(!empty($newImages))
            <div class="row mb-3">
                <div class="col-12">
                    <label class="form-label">Anteprima immagini</label>
                    <div class="row my-border rounded shadow py-4">
                        @foreach($newImages as $key => $newImage)
                            <div class="col my-3">
                                <div class="img-preview mx-auto shadow" style="background-image: url({{$newImage->temporaryUrl()}}); background-position:center;"></div>
                                <button type="button" class="btn btn-card shadow d-block text-center mt-3 mx-auto" wire:click="removeImage({{$key}})">
                                    Cancella
                                </button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="d-flex">
            <button type="submit" class="form-button">Modifica</button>
            <p class="mt-5 text-muted">oppure</p>
            <button type="button" wire:click="article_destroy" class="form-button">Elimina</button>
        </div>
    </form>
</div>
