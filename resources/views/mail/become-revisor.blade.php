<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Presto.it</title>
</head>
<body>
    <h2>Un utente ha richiesto di diventare revisore</h2>
    <p><span class="fw-bold fs-3">Nome: </span>{{$user->name}}</p>
    <p><span class="fw-bold fs-3">Email: </span>{{$user->email}}</p>
    <p><span class="fw-bold fs-3">Raccontaci di te: </span>{{$description}}</p>
    <h3 class="fw-bold">Per rendere revisore:</h3>
    <a href="{{route('make.revisor', compact('user'))}}">Rendi revisore</a>
</body>
</html>