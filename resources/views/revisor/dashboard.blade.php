<x-layout>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>{{$article_to_check ? "Ecco l'annuncio da revisionare" : "Non ci sono annunci da revisionare"}}</h2>
            </div>
        </div>
    </div>
    @if($article_to_check)
        @foreach ($article_to_check as $article)
            <div class="container my-5">
                <div class="row">
                    <div class="col-12" style="overflow-x:auto;">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Titolo</th>
                                <th scope="col">Categoria</th>
                                <th scope="col">Utente</th>
                                <th scope="col">Accetta</th>
                                <th scope="col">Rifiuta</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$article->id}}</td>
                                    <td>{{$article->title}}</td>
                                    <td>{{$article->category->name}}</td>
                                    <td>{{$article->user->name}}</td>
                                    <td>
                                        <form action="{{route('revisor.accept-article', ['article' => $article])}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-card">Accetta</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{route('revisor.reject-article', ['article' => $article])}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-card">Rifiuta</button>
                                        </form>
                                    </td>
                                <tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
                <div class="row mt-5 ">
                    <div class="col-12 col-md-4 text-center">
                        <p class="fw-bold ">Immagini</p>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <p class="fw-bold ">Tag</p>
                    </div>
                    <div class="col-12 col-md-4 text-center">
                        <p class="fw-bold ">Revisione Immagini</p>
                    </div>
                    @forelse ($article->images as $image)
                        <div class="row myborder my-3">
                            <div class="col-12 col-md-4 my-4 d-flex justify-content-center">
                                <img src="{{$image->getUrl(600, 600)}}" alt="" class="w-50 my-3 shadow">
                            </div>
                            <div class="col-12 col-md-4 my-4 d-flex justify-content-center">
                                @if($image->labels)
                                    <ul>
                                        @foreach($image->labels as $label)
                                            <li class="list-unstyled">- {{$label}}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <div class="col-12 col-md-4 my-4 d-flex flex-column align-items-center justify-content-center">
                                <p>Adulti: <span class="{{$image->adult}}"></span></p>
                                <p>Satira: <span class="{{$image->spoof}}"></span></p>
                                <p>Medicina: <span class="{{$image->medical}}"></span></p>
                                <p>Violenza: <span class="{{$image->violence}}"></span></p>
                                <p>Spinto: <span class="{{$image->racy}}"></span></p>
                            </div>
                        </div>
                    @empty
                        <div class="row myborder my-3">
                            <div class="col-12 col-md-4 my-4 d-flex justify-content-center">
                                <img src="{{Storage::url( './../../img/default-image.jpg' )}}" alt="" class="w-50 my-3 shadow">
                            </div>
                            <div class="col-12 col-md-4 my-4 text-center">
                                <p>Non è stata caricata nessuna immagine per questo articolo.</p>
                                <p>È stata assegnata un'immagine di default e non sono stati generati dei tag.</p>
                            </div>
                            <div class="col-12 col-md-4 my-4 d-flex flex-column align-items-center justify-content-center">
                                <p>Adulti: X</p>
                                <p>Satira: X</p>
                                <p>Medicina: X</p>
                                <p>Violenza: X</p>
                                <p>Spinto: X</p>
                            </div>
                        </div>
                    @endforelse
                </div>
            </div>
        @endforeach
    @endif
    <div class="d-flex justify-content-center">{{$article_to_check->links()}}</div>
</x-layout>