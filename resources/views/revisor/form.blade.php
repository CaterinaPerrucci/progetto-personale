<x-layout>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="form">
                <span class="form-title">Diventa revisore</span>
                <form action="{{route('become.revisor')}}">
                    @csrf
                    <div class="mb-3 form-input">
                        <i class="fa-regular fa-user"></i>
                        <input value="{{Auth::user()->name}}" name="name" type="text" class="form-control" placeholder="Nome">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <i class="fa-regular fa-envelope"></i>
                        <input value="{{Auth::user()->email}}" type="email" name="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="bar"></span>
                    </div>
                    <div class="mb-3 form-input">
                        <textarea name="description" id="" cols="30" rows="5" class="form-control" placeholder="Raccontaci di te..."></textarea>
                    </div>
                    <button type="submit" class="form-button">Invia richiesta</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>