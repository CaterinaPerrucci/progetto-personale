<x-layout>
    <div class="container my-5">
        <div class="row">
            <h2>{{__('ui.lastProducts')}}</h2>
        </div>
    </div>
    <div class="container my-5">
        <div class="row justify-content-center">
            @forelse($articles as $article)
                <div class="col-12 d-flex justify-content-center col-md-3 m-5">
                    <x-card :article="$article"/>
                </div>
            @empty
                <div class="col-12">
                    <h4>Non ci sono annunci caricati</h4 class="card-title">
                    @auth
                    <p>Pubblicane uno: <a href="{{route('article.create')}}" class="card-title">Inserisci annuncio</a></p>
                    @endauth
                </div>
            @endforelse
        </div>
    </div>
    @auth
        @if (!Auth::user()->is_revisor)
            <div class="container my-5">
                <div class="row">
                    <h6>Vuoi lavorare con noi? <a href="{{route('become.revisor-form')}}" class="card-title">Vai</a> al form per richiedere di diventare un nostro revisore.</h6>
                </div>
            </div>
        @endif
    @endauth
</x-layout>