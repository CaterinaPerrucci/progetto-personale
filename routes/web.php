<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;


Route::get('/', [PublicController::class, 'home'])->name('welcome');
Route::get('/category/{category}', [PublicController::class, 'categoryShow'])->name('category.show');
Route::get('/research/article', [PublicController::class, 'searchArticles'])->name('article.search');
Route::post('/language/{lang}', [PublicController::class, 'setLanguage'])->name('set_language_locale');

Route::get('/article/create', [ArticleController::class, 'create'])->middleware('auth')->name('article.create');
Route::get('/article/show/{article}', [ArticleController::class, 'articleShow'])->name('article.show');
Route::get('/article/index', [ArticleController::class, 'articleIndex'])->name('article.index');

Route::get('/revisor/dashboard', [RevisorController::class, 'dashboard'])->middleware('isRevisor')->name('revisor.dashboard');
Route::patch('/accept/article/{article}', [RevisorController::class, 'acceptArticle'])->middleware('isRevisor')->name('revisor.accept-article');
Route::patch('/reject/article/{article}', [RevisorController::class, 'rejectArticle'])->middleware('isRevisor')->name('revisor.reject-article');
Route::patch('/review/article({article}', [RevisorController::class, 'reviewArticle'])->middleware('isRevisor')->name('revisor.review-article');
Route::get('/become/revisor', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become.revisor');
Route::get('/make/revisor/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');
Route::get('/become/revisor/form', [RevisorController::class, 'becomeRevisorForm'])->middleware('auth', 'notRevisor')->name('become.revisor-form');

Route::get('/login/github', [PublicController::class, 'redirectToProvider']);
Route::get('/login/github/callback', [PublicController::class, 'handleProviderCallback']);

Route::get('/login/google', [PublicController::class, 'google']);
Route::get('/login/google/callback', [PublicController::class, 'googleCallback']);


Route::get('/article/edit/{article}', [ArticleController::class, 'edit'])->middleware('auth')->name('article.edit');

Route::get('/user/dashboard/{user}', [PublicController::class, 'dashboard'])->middleware('auth')->name('user.dashboard');

Route::get('cart', [CartController::class, 'cartList'])->name('cart.list');
Route::post('cart', [CartController::class, 'addToCart'])->name('cart.store');
Route::post('update-cart', [CartController::class, 'updateCart'])->name('cart.update');
Route::post('remove', [CartController::class, 'removeCart'])->name('cart.remove');
Route::post('clear', [CartController::class, 'clearAllCart'])->name('cart.clear');